﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssetBundles ;

namespace  MiniGame.Asset {
	
public class AssetBundleController : MonoBehaviour {




		#region for Fields

		public static AssetBundleController Reference;

		private bool isDownloadRequested ;


		#endregion


		#region for Property
		public bool IsDownloadRequested { get { return (isDownloadRequested); } set{ isDownloadRequested = value; } }
		#endregion


		#region for Unity Function

		// Use this for initialization
		protected internal IEnumerator Start()
		{
			
			yield return StartCoroutine(Initialize());
		}

		//Awake instance
		protected void Awake(){

			//Single tone reference 
			if (Reference == null)
				Reference = this;
		}

		#endregion





		#region For common Function

		//This method will load the sene
		public IEnumerator LoadLevelFromAssetBundle(string levelName,string fromAssetBundle){
			// Load level.
			if(Application.internetReachability != NetworkReachability.NotReachable )
			yield return StartCoroutine(InitializeLevelAsync(levelName,fromAssetBundle, false));

		}


		// Initialize the downloading URL.
		// eg. Development server / iOS ODR / web URL
		protected virtual void InitializeSourceURL()
		{
			/*
							// If ODR is available and enabled, then use it and let Xcode handle download requests.
			#if ENABLE_IOS_ON_DEMAND_RESOURCES
			if (UnityEngine.iOS.OnDemandResources.enabled)
			{
			AssetBundleManager.SetSourceAssetBundleURL("odr://");
			return;
			}
			#endif
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			// With this code, when in-editor or using a development builds: Always use the AssetBundle Server
			// (This is very dependent on the production workflow of the project.
			//      Another approach would be to make this configurable in the standalone player.)
			AssetBundleManager.SetDevelopmentAssetBundleServer();
			return;
			#else
			// Use the following code if AssetBundles are embedded in the project for example via StreamingAssets folder etc:
			AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + "/");
			// Or customize the URL based on your deployment or configuration
			//AssetBundleManager.SetSourceAssetBundleURL("http://www.MyWebsite/MyAssetBundles");
			return;
			#endif
			*/

			//TODO:
			//This is only for Android
			AssetBundleManager.SetSourceAssetBundleURL("https://altranab.000webhostapp.com/Android");

		}

		// Initialize the downloading url and AssetBundleManifest object.
		protected virtual IEnumerator Initialize()
		{
			// Don't destroy the game object as we base on it to run the loading script.

			DontDestroyOnLoad(gameObject);

			InitializeSourceURL();

			// Initialize AssetBundleManifest which loads the AssetBundleManifest object.
			var request = AssetBundleManager.Initialize();

			if (request != null)
				yield return StartCoroutine(request);
		}


		//let the level load
		protected virtual IEnumerator InitializeLevelAsync(string levelName,string fromAssetBundle, bool isAdditive) {

			// This is simply to get the elapsed time for this phase of AssetLoading.
			float startTime = Time.realtimeSinceStartup;

			// Load level from assetBundle.
			AssetBundleLoadOperation request = AssetBundleManager.LoadLevelAsync(fromAssetBundle, levelName, isAdditive);
			if (request == null)
				yield break;
			
			yield return StartCoroutine(request);

			// Calculate and display the elapsed time.
			float elapsedTime = Time.realtimeSinceStartup - startTime;
			Debug.Log("Finished loading scene " + levelName + " in " + elapsedTime + " seconds");




		}


		//This method helps you load a particular object from the asset bundle 
		protected virtual IEnumerator InstantiateGameObjectAsync(string assetBundleName, string assetName) {
			// This is simply to get the elapsed time for this phase of AssetLoading.
			float startTime = Time.realtimeSinceStartup;

			// Load asset from assetBundle.
			AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject));
			if (request == null)
				yield break;
			yield return StartCoroutine(request);

			// Get the asset.
			GameObject prefab = request.GetAsset<GameObject>();

			if (prefab != null)
				GameObject.Instantiate(prefab);

			// Calculate and display the elapsed time.
			float elapsedTime = Time.realtimeSinceStartup - startTime;
			Debug.Log(assetName + (prefab == null ? " was not" : " was") + " loaded successfully in " + elapsedTime + " seconds");
		}


		#endregion
	}



}
