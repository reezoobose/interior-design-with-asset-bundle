﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AssetBundles;
using MiniGame.Asset ;
using System ;

namespace MiniGame.UI
{
	public class MainMaenuUIController : MonoBehaviour
	{

		#region for Fields

		public string NameOFExitButton;
		public string NameOFEneterButton;



		public string AssetBundleNameToloadScene;
		public string SceneName;


		public GameObject DwonloadPanel;
		public GameObject NOInternetPanel;

		public Text MessageText ;

		#endregion


		#region for Property

		 
		 Button Exitbutton { get { return (GameObject.Find(NameOFExitButton).GetComponent<Button>()); } }
		 Button Enterbutton { get { return (GameObject.Find(NameOFEneterButton).GetComponent<Button>()); } }

		#endregion


		#region For unity Message System

		//internal do not allow to inherit from another class not in same assembly .
		protected internal virtual void Awake()
		{

			if (Exitbutton)
			{
				Exitbutton.onClick.RemoveAllListeners();
				Exitbutton.onClick.AddListener(() => OnExitButtonClick());
			}	

			if (Enterbutton)
			{
				Enterbutton.onClick.RemoveAllListeners();
				Enterbutton.onClick.AddListener(() => OnEnterButtonClick());
			}	
			
		}


		#endregion

		#region for Functions


		private void OnEnterButtonClick()
		{
			if (Application.internetReachability != NetworkReachability.NotReachable) {
				try{
				MessageText.text ="Dwonloading....";
				DwonloadPanel.SetActive (true);
				IEnumerator levelLoadCoroutine = AssetBundleController.Reference.LoadLevelFromAssetBundle (this.SceneName, this.AssetBundleNameToloadScene);
				StartCoroutine (levelLoadCoroutine);
				}
				catch(Exception e){
					if (e.Equals( LogType.Exception)) {
						Debug.Log ("Error take place");
						MessageText.text = "Dwonload Error Try Again.....";
					}

				}
				finally{
					StopAllCoroutines ();
					Invoke ("DeactivateDownloadPanelWithDelay", 1.5f);
				}
			} else {
				NOInternetPanel.SetActive (true);
				Invoke("DeactivateNointernetPanelWithDelay",2f);

			}

		}



		private void OnExitButtonClick()
		{
			Debug.Log("Exit button is clicked");
			//TODO: It is better to give a popup that do you want to quit ?
			Application.Quit();

		}


		private void DeactivateNointernetPanelWithDelay(){
			
			NOInternetPanel.SetActive (false);
			CancelInvoke ("DeactivateNointernetPanelWithDelay");
		}


		private void DeactivateDownloadPanelWithDelay(){

			DwonloadPanel.SetActive (false);
			CancelInvoke ("DeactivateDownloadPanelWithDelay");
		}
		#endregion
	}


}
