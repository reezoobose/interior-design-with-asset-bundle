﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement ;
using MiniGame.Asset ;

namespace MiniGame.UI {
	
public class UiController : MonoBehaviour {

	#region field and property

	public string NameOFExitButton="Exit_Button";
	public Button Exitbutton;

	public string NameOFFarnitureButton="Farniture";
	public Button Farniturebutton;

	public string NameOFChairButton="Chair";
	public Button Chairbutton;

	public string NameOFSofaButton="Sofa";
	public Button Sofabutton; 

	public string NameOFcolorbutton1="reezoo1"; //connected to mat 1
	public Button colorbutton1 ;

	public string NameOFcolorbutton2="reezoo2";//connected to mat 2
	public	Button colorbutton2;

	public string NameOFcolorbutton3="reezoo3";//connected to mat 3
	public Button colorbutton3;

	public Material[ ] ArrayofMaterial = new Material[3] ;

	public GameObject MySofa;
	public GameObject MyChair;
	public GameObject ColorPanel;
	public GameObject FarniturePanel;
	public GameObject LoadingPanel;

	#endregion

	#region For Unity Function

	private void Awake(){

		Init ();
	}

	#endregion

	#region For common Function
	public void OnfarnitureButtonClick(){

		//stop color panel
		ColorPanel.SetActive(false);
		//on farniture panel
		FarniturePanel.SetActive(true);


	}


	public void OnSofaButtonClick(){

		//hide farniture panel
		FarniturePanel.SetActive(false);
		//activate color panel
		ColorPanel.SetActive(true);
		//Activale panel
		LoadingPanel.SetActive(true);
		//Random time of dwonload
		float dwonloadTime = Random.Range(1f,3f);
		Invoke ("ActivateSofa", dwonloadTime);
		//Deactivate sofa button
			Sofabutton.interactable=false;
		
	}
	
	public void  ActivateChair(){

			//Deactivale panel
			LoadingPanel.SetActive(false);
			//Activate sofa
			MyChair.SetActive(true);
			//cancell invoke
			CancelInvoke("ActivateChair");
	}

	public void  ActivateSofa(){
			//Deactivale panel
			LoadingPanel.SetActive(false);
			//Activate sofa
			MySofa.SetActive(true);
			//cancell invoke
			CancelInvoke("ActivateSofa");
	}

	public void OnChairButtonClick(){

		//hide farniture panel
		FarniturePanel.SetActive(false);
		//activate color panel
		ColorPanel.SetActive(true);
			//Activale panel
			LoadingPanel.SetActive(true);
			//Random time of dwonload
			float dwonloadTime = Random.Range(1f,3f);
			Invoke ("ActivateChair", dwonloadTime);
			//Deactivate chair button
			Chairbutton.interactable = false ;
		
	}

	public void OnExitButtonClick()
	{
		SceneManager.LoadScene ("Menu Scene");
	}

	public void OnColorButtonClicked(int index){

		MySofa.GetComponent<MeshRenderer> ().material = ArrayofMaterial [index];
		MyChair.GetComponent<MeshRenderer> ().material = ArrayofMaterial [index];

			
	}



	public void Init(){


		Exitbutton = GameObject.Find (NameOFExitButton).GetComponent<Button> ();
		Farniturebutton = GameObject.Find (NameOFFarnitureButton).GetComponent<Button> ();
		Chairbutton = GameObject.Find (NameOFChairButton).GetComponent<Button> ();
		Sofabutton = GameObject.Find (NameOFSofaButton).GetComponent<Button> ();
		colorbutton1 = GameObject.Find (NameOFcolorbutton1).GetComponent<Button> ();
		colorbutton2 = GameObject.Find (NameOFcolorbutton2).GetComponent<Button> ();
		colorbutton3 = GameObject.Find (NameOFcolorbutton3).GetComponent<Button> ();


		MySofa = GameObject.Find ("MySofa") ;
		MyChair = GameObject.Find ("MyChair");
		ColorPanel = GameObject.Find ("Color_Panel");
		FarniturePanel = GameObject.Find ("FarniturePanel");
		LoadingPanel = GameObject.Find ("LoadingPanelAssetScene");
		
		Exitbutton.onClick.RemoveAllListeners ();
		Exitbutton.onClick.AddListener (() => OnExitButtonClick());

		Farniturebutton.onClick.RemoveAllListeners ();
		Farniturebutton.onClick.AddListener (() => OnfarnitureButtonClick());

		Chairbutton.onClick.RemoveAllListeners ();
		Chairbutton.onClick.AddListener (() => OnChairButtonClick ());

		Sofabutton.onClick.RemoveAllListeners() ;
		Sofabutton.onClick.AddListener (() => OnSofaButtonClick ());

		colorbutton1.onClick.RemoveAllListeners() ;
		colorbutton1.onClick.AddListener (() => OnColorButtonClicked (0));

		colorbutton2.onClick.RemoveAllListeners() ;
		colorbutton2.onClick.AddListener (() => OnColorButtonClicked (2));

		colorbutton3.onClick.RemoveAllListeners() ;
		colorbutton3.onClick.AddListener (() => OnColorButtonClicked (1));

			FarniturePanel.SetActive (false);
			MySofa.SetActive (false);
			MyChair.SetActive (false);
			LoadingPanel.SetActive (false);

	}

	#endregion
	}
}
